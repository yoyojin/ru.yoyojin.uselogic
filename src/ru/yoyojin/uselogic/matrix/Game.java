package ru.yoyojin.uselogic.matrix;

import java.util.Hashtable;
import java.util.Map;

public class Game{

	public static int SIZE_MATRIX_H = 10;
	public static int SIZE_MATRIX_V = 10;

	/**
	 * Playground is a matrix that contains information about cells state {@link CellStates}.
	 */
	private int[][] playgroundMatrix;

	/**
	 * One segment is an array of captured cells that touching each other.
	 * Segment may consist of one captured cell.
	 */
	private int[][] capturedSegmentMatrix;

	/**
	 * This matrix accumulates information about segments active state
	 */
	private Map<Integer, Boolean> capturedSegmentStateMatrix;

	public Game() {

	}

	public void initializeGame() {
		playgroundMatrix = new int[SIZE_MATRIX_H][SIZE_MATRIX_V];
		capturedSegmentMatrix = new int[SIZE_MATRIX_H][SIZE_MATRIX_V];
		capturedSegmentStateMatrix = new Hashtable<>();
		dropGameMatrix();
	}

	private void dropGameMatrix() {
		for (int i = 0; i < SIZE_MATRIX_H; i++) {
			for (int j = 0; j < SIZE_MATRIX_V; j++) {
				playgroundMatrix[i][j] = CellStates.EMPTY_CELL;
				capturedSegmentMatrix[i][j] = CellStates.EMPTY_CELL;
			}
		}
		capturedSegmentStateMatrix.clear();
	}

	/**
	 * Checking segment state on activity. Segment is active if have the PLAYER_ACTIVE_CELL cell near.
	 * @param segmentNumber - number of segment that must be checked on active state
	 * @return true - if segment is active, false - if segment is inactive or not be found.
	 */
	public boolean isSegmentActive(Integer segmentNumber){
		return capturedSegmentStateMatrix.get(segmentNumber);
	}

	
	public void saveGameState() {

	}
}
