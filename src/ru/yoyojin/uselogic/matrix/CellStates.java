package ru.yoyojin.uselogic.matrix;

/**
 * Describes the activity status and identity which the cell can be.
 * 
 * @author AKorotkov
 *
 */
public interface CellStates {
	/**
	 * A cell that belongs to nobody. Any player may to captured on this cell if
	 * the cell(s) has its own status is active and is near.
	 */
	public static final int EMPTY_CELL = 0;

	/**
	 * {@code EMPTY_CELL} which was captured by first player. Cell belongs to
	 * first player and may be captured by the second player if the cell(s) has
	 * its own status is active and is near. This cell has always active status.
	 */
	public static final int FIRST_PLAYER_ACTIVE_CELL = 1;
	/**
	 * Similarly a {@code FIRST_PLAYER_CELL}.
	 */
	public static final int SECOND_PLAYER_ACTIVE_CELL = -1;

	/**
	 * The {@code SECOND_PLAYER_CELL} which was captured by first player from
	 * second player. This cell will always belong first player. This cell may
	 * have active or inactive status. Cell is active if have the friendly active cell near.
	 */
	public static final int FIRST_PLAYER_CAPTURED_CELL = 2;
	/**
	 * Similarly a {@code FIRST_PLAYER_CAPTURED_CELL}.
	 */
	public static final int SECOND_PLAYER_CAPTURED_CELL = -2;
}
